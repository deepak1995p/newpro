import React, { Component } from 'react';
import { View, Text,Image,Alert } from 'react-native';
import { Container, Content,Header,Form,Item,Input,Button, Label } from 'native-base';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import Details from './Details';
import Nextpage from './Nextpage';
import Det from './Det';
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username:"deepakpatel",
      password:"123456",
    };
  }
  myValidate=()=>{
    const {username,password}=this.state;
    if(username=="" && password==""){
      Alert.alert('Please fill the Username and Password')
    }
    else if(username != "deepakpatel"&& password !="123456"){
      Alert.alert('Account does not exist')
    }
    else if (username=="deepakpatel"&& password==""){
      Alert.alert('password empty')
    }
    else if (username==""&& password=="123456"){
      Alert.alert('username empty')
    }
    else if (username=="deepakpatel" && password=="123456"){
      this.props.navigation.navigate('Nextpage')
    
    }
    else{
      Alert.alert('either username or password is incorrect');
    }
  }

  render() {
    return (
      <Container padder
      style={{backgroundColor:"skyblue"}}
      >
        {/* <Text style={{ backgroundColor:'blue'}}>
          <Text style={{fontSize:20,color:'white',alignSelf:'center'}}>Login Page</Text>
        </Text> */}
        <Content padder>
          <Image style={{alignSelf:'center',width: 180, height: 180}}
          source={require("./loginn.png")} />
          <Form >
              <Item floatingLabel >
                <Label>Username</Label>
                  <Input 
               
                  onChangeText={(username) => this.setState({username:username})}
                />
                
                </Item>
                <Item floatingLabel >
                <Label>Password</Label>
                  <Input secureTextEntry
               
                  onChangeText={(password) => this.setState({password:password})}
                />
                </Item>
            </Form>
           
          <Button  warning style={{alignSelf: 'center',borderTopRightRadius:5,borderBottomLeftRadius:5,padding:15,marginTop:15}}

            onPress={() => this.myValidate()}
          >
            <Text style={{fontSize:20}}>Login</Text>
          </Button>
          

        </Content>
      </Container>
    )
    
    
  }
}

const navigation=createSwitchNavigator({
  App,
  Nextpage,
  Det ,
  Details
})  
export default createAppContainer(navigation); 

