import React, { Component } from 'react';
import { View, Text,Image, TouchableOpacity} from 'react-native';
import{Container,Button,Icon,Content,Item,Header,Left} from 'native-base';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
class Nextpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container padder>
        <Header>
        <TouchableOpacity
              onPress={() => this.props.navigation.navigate('App')}
            >
              <Icon style={{color:'white',flexDirection:'row'}} 
              name="arrow-back" />
              <Text>Back</Text>
            </TouchableOpacity>
        </Header>
      <Content style={{flex:1}}>
      <View style={{flex:1,backgroundColor:'white',marginTop:10}}>
        <View style={{flexDirection:'row-reverse',marginTop:10}}>
          <Icon style={{color:'lightgray',marginRight:10}} 
              name="wifi" />
          <Icon style={{color:'lightgray',marginRight:8}} 
              name="settings" />
             </View>
          <Image style={{alignSelf:'center',width: 150, height: 150}}
          source={require("./robot.png")} />
        <Text style={{alignSelf:'center',fontSize:15}}> Robot 0</Text>
        <View style={{flexDirection:'row',borderBottomWidth:1}}>
          <View style={{flex:1, alignItems:'center', justifyContent:'center', borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <Text style={{color:'lightgray'}}>Serial Number:</Text>
            <Text style={{color:'gray'}}>34567897</Text>
          </View>
          <View style={{flex:1, alignItems:'center', justifyContent:'center',borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('Det')}
            >
              <Icon style={{color:'green'}} 
              name="settings" />
              <Text style={{color:'green'}}>Running</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{flex:1,backgroundColor:'white',marginTop:10}}>
        <View style={{flexDirection:'row-reverse',marginTop:10}}>
          <Icon style={{color:'lightgray',marginRight:10}} 
              name="wifi" />
          <Icon style={{color:'lightgray',marginRight:8}} 
              name="settings" />
             </View>
          <Image style={{alignSelf:'center',width: 150, height: 150}}
          source={require("./robot.png")} />
        <Text style={{alignSelf:'center',fontSize:15}}> Robot 0</Text>
        <View style={{flexDirection:'row',borderBottomWidth:1}}>
          <View style={{flex:1, alignItems:'center', justifyContent:'center', borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <Text style={{color:'lightgray'}}>Serial Number:</Text>
            <Text style={{color:'gray'}}>35867897</Text>
          </View>
          <View style={{flex:1, alignItems:'center', justifyContent:'center',borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('Det')}
            >
              <Icon style={{color:'green'}} 
              name="settings" />
              <Text style={{color:'green'}}>Running</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{flex:1,backgroundColor:'white',marginTop:10}}>
        <View style={{flexDirection:'row-reverse',marginTop:10}}>
          <Icon style={{color:'lightgray',marginRight:10}} 
              name="wifi" />
          <Icon style={{color:'lightgray',marginRight:8}} 
              name="settings" />
             </View>
          <Image style={{alignSelf:'center',width: 150, height: 150}}
          source={require("./robot.png")} />
        <Text style={{alignSelf:'center',fontSize:15}}> Robot 0</Text>
        <View style={{flexDirection:'row',borderBottomWidth:1}}>
          <View style={{flex:1, alignItems:'center', justifyContent:'center', borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <Text style={{color:'lightgray'}}>Serial Number:</Text>
            <Text style={{color:'gray'}}>36997897</Text>
          </View>
          <View style={{flex:1, alignItems:'center', justifyContent:'center',borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('Det')}
            >
              <Icon style={{color:'green'}} 
              name="settings" />
              <Text style={{color:'green'}}>Running</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{flex:1,backgroundColor:'white',marginTop:10}}>
        <View style={{flexDirection:'row-reverse',marginTop:10}}>
          <Icon style={{color:'lightgray',marginRight:10}} 
              name="wifi" />
          <Icon style={{color:'lightgray',marginRight:8}} 
              name="settings" />
             </View>
          <Image style={{alignSelf:'center',width: 150, height: 150}}
          source={require("./robot.png")} />
        <Text style={{alignSelf:'center',fontSize:15}}> Robot 0</Text>
        <View style={{flexDirection:'row',borderBottomWidth:1}}>
          <View style={{flex:1, alignItems:'center', justifyContent:'center', borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <Text style={{color:'lightgray'}}>Serial Number:</Text>
            <Text style={{color:'gray'}}>35567557</Text>
          </View>
          <View style={{flex:1, alignItems:'center', justifyContent:'center',borderRightWidth:1, borderRightColor:'lightgray',borderBottomWidth:1,borderBottomColor:'lightgray'}}>
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('Det')}
            >
              <Icon style={{color:'green'}} 
              name="settings" />
              <Text style={{color:'green'}}>Running</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
              </Content>
              </Container>
    );
  }
}



export default Nextpage;
